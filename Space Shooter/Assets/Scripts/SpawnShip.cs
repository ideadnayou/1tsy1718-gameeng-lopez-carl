﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SpawnShip : MonoBehaviour {

    public GameObject ship;
    public float life = 3;
    private GameObject spawnedShip;

	// Use this for initialization
	void Start ()
    {
        spawnedShip = Instantiate(ship) as GameObject;
        //spawnedShip.GetComponent<PlayerMovement>().spawnController = this;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (life >= 5) life = 5;

        if (life <= 0)
        {
            Destroy(spawnedShip);
            SceneManager.LoadScene("GameOverScene");
        }
	}
}
