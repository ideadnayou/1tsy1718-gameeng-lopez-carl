﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnObjects  : MonoBehaviour {

    public GameObject spawnObject;
    public GameObject spawnHealth;

	// Use this for initialization
	void Start ()
    {
        InvokeRepeating("Spawn", 0.5f, Random.Range(0.5f,2));
        InvokeRepeating("SpawnPowerUp", 5.0f, Random.Range(5.0f, 10.0f));
	}
	
	// Update is called once per frame
	void Update ()
    {
        
	}

    void Spawn()
    {
        float rand = Random.Range(-7, 7);

        Instantiate(spawnObject, new Vector3(rand, 8, spawnObject.transform.position.z),transform.rotation);
    }

    void SpawnPowerUp()
    {
        float rand = Random.Range(-7, 7);

        Instantiate(spawnHealth, new Vector3(rand, 8, spawnObject.transform.position.z), transform.rotation);
    }
}
