﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    public SpawnShip spawnController;

    public float speed;
    public float bulletSpeed;

    public GameObject bullet;

	// Use this for initialization
	void Start ()
    {
       spawnController = (SpawnShip)GameObject.FindObjectOfType<SpawnShip>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow))
        {
            this.transform.Translate(new Vector3(0,speed,0) * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
        {
            this.transform.Translate(new Vector3(-speed, 0, 0) * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow))

        {
            this.transform.Translate(new Vector3(0, -speed, 0) * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
        {
            this.transform.Translate(new Vector3(speed, 0, 0) * Time.deltaTime);
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameObject bulletObject1 = Instantiate(bullet, this.transform.position + new Vector3(0.3f, 1, 0), this.transform.rotation);
            bulletObject1.GetComponent<Rigidbody>().velocity = Vector3.up * bulletSpeed;

            GameObject bulletObject2 = Instantiate(bullet, this.transform.position + new Vector3(-0.3f,1,0), this.transform.rotation);
            bulletObject2.GetComponent<Rigidbody>().velocity = Vector3.up * bulletSpeed;

            DestroyObject(bulletObject1.gameObject, 5);
            DestroyObject(bulletObject2.gameObject, 5);
        }

        this.transform.position = new Vector3(Mathf.Clamp(this.transform.position.x, -7, 7), Mathf.Clamp(this.transform.position.y, -4, 6), transform.position.z);
       
    }

}
