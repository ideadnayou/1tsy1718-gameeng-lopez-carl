﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour {

    public float InvulnerabilityTime = 3.0f;
    public float BlinkInterval = 0.2f;

    private bool invulnerable;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Contains("Asteriod") && this.gameObject.name.Contains("SpaceShip") && !invulnerable)
        {
            Destroy(col.gameObject);
            StartCoroutine(Blink());
            this.GetComponent<PlayerMovement>().spawnController.life--;
        }
        else if (col.gameObject.name.Contains("Asteriod") && this.gameObject.name.Contains("Bullet"))
        {
            Destroy(col.gameObject);
            Destroy(this.gameObject);
        }
        else if (col.gameObject.name.Contains("PowerUp") && this.gameObject.name.Contains("SpaceShip"))
        {
           Destroy(col.gameObject);
           this.GetComponent<PlayerMovement>().spawnController.life++;
        }

    }

    IEnumerator Blink()
    {
        //int cnt = 3;
        //this.GetComponent<BoxCollider>().enabled = false;

        //while (cnt > 0)
        //{
        //    if (this.GetComponent<MeshRenderer>().enabled)
        //        this.GetComponent<MeshRenderer>().enabled = false;
        //    yield return new WaitForSeconds(.2f);
        //    this.GetComponent<MeshRenderer>().enabled = true;
        //    yield return new WaitForSeconds(.2f);
        //    cnt--;
        //}

        //this.GetComponent<BoxCollider>().enabled = true;

        invulnerable = true;
        int totalBlinks = Mathf.FloorToInt(InvulnerabilityTime / BlinkInterval);

        MeshRenderer mesh = this.GetComponent<MeshRenderer>();
        for (int i = 0; i < totalBlinks; i++)
        {
            mesh.enabled = !mesh.enabled;
            yield return new WaitForSeconds(BlinkInterval);
        }

        mesh.enabled = true;

        invulnerable = false;
    }
}
