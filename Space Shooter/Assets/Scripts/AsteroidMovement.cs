﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour
{
    // Use this for initialization
    void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        this.transform.Translate(new Vector3(0, Random.Range(-20,-5), 0) * Time.deltaTime);
        Destroy(this.gameObject, 4.0f);

    }
}