﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debuff : MonoBehaviour
{
    public float duration = 10;

    protected virtual void Update()
    {
        duration -= Time.deltaTime;
        if (duration > 0)
        {
            Effect();
        }
        else if (duration <= 0)
        {
            Destroy(this);
        }
    }

    public virtual void Effect()
    {

    }
}
