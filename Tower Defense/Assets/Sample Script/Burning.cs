﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Burning : Debuff
{
    public float burnDamage;
    float tickDamage = 1;

    protected override void Update()
    {
        tickDamage -= Time.deltaTime;
        if (tickDamage <= 0)
        {
            Effect();
            tickDamage = 1;
        }
        else if (this.GetComponent<MonsterAttribute>().isDead)
        {
            Destroy(this);
        }
    }

    public override void Effect()
    {
        burnDamage = Random.Range(5, 10);

        this.GetComponent<MonsterAttribute>().takeDamage(burnDamage);
    }
}