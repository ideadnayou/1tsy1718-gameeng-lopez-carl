﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Slow : Debuff
{
    NavMeshAgent agent;
    public float slowedEffect = 2;
    public float initSpeed;

    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        initSpeed = agent.speed;
    }

    protected override void Update()
    {
        base.Update();
    }


    public override void Effect()
    {
        agent.speed = slowedEffect;
    }

    void OnDestroy()
    {
        agent.speed = initSpeed;
    }
}
