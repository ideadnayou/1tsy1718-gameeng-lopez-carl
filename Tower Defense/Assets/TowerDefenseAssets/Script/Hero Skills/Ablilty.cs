﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Ablilty : ScriptableObject
{
    public string skillName;
    public float skillCD;
    public int damage;
    public float range;

    public abstract void Initialize(GameObject obj);
    public abstract void TriggerAbility();
    public abstract void Effect();
}