﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public float maxLives = 100;
    public float currentLives;
    public int playerGold = 500;
    public float hpFill;

    public Text goldText;
    public Text waveText;
    public Image healthBar;

    public bool isDead = false;

    public MonsterSpawner spawnControl;

    // Use this for initialization
    void Start()
    {
        currentLives = maxLives;
        goldText.text = playerGold.ToString();
        spawnControl = (MonsterSpawner)GameObject.FindObjectOfType<MonsterSpawner>();
    }

    // Update is called once per frame
    void Update()
    {
        if (currentLives <= 0)
        {
            isDead = true;
            currentLives = 0;
        }

        if (playerGold <= 0) playerGold = 0;

        UpdateHealth();
        UpdateGold();
        UpdateWave();

    }

    void UpdateHealth()
    {
        hpFill = currentLives / maxLives;
        healthBar.fillAmount = hpFill;
    }

    void UpdateGold()
    {
        goldText.text = playerGold.ToString();
    }

    void UpdateWave()
    {
        waveText.text = ("WAVE " + spawnControl.waveCount.ToString());
    }
}
