﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class HeroSpawner : MonoBehaviour {

    CharacterData hData;
    Scene Test;
    
	void Awake()
    {
        hData = GameObject.FindObjectOfType<GameData>().mData;
        Test = SceneManager.GetSceneByName("SampleRatio");

        GameObject hero = Instantiate(hData.heroPrefab) as GameObject;
        hero.GetComponent<HeroAttribute>().SetAttribute(hData);

        SceneManager.MoveGameObjectToScene(hero.gameObject, Test);
        hero.transform.position = this.transform.position;
        hero.GetComponent<NavMeshAgent>().enabled = true;

    }
}