﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum MonsterType
{
    Flying,
    Ground,
}

[System.Serializable]
public class MonsterData
{
    public string name;
    public MonsterType type;
    public GameObject monsterPrefab;
    public float monsterHp;
    public int monsterGold;
    public int monsterCount;
}

public class MonsterSpawner : Singleton<MonsterSpawner>
{
    List<GameObject> ObjectList = new List<GameObject>();

    [Header("Enemy Data")]
    public List<MonsterData> monsterList;
    public List<MonsterData> bossList;

    [Header("Spawner Settings")]
    public float spawnInterval = 0;
    public float countdown = 0;
    public int spawnCount = 0;
    public int enemyIndex = 0;
    public int waveCount = 0;

    // Update is called once per frame
    void Update()
    {
        if (spawnCount < 0) spawnCount = 0;
        if (spawnCount > 0) return;

        if (countdown <= 0)
        {
            waveCount++;
            StartCoroutine(SpawnMonster());
            countdown = spawnInterval;

            return;
        }

        if (enemyIndex >= monsterList.Count) enemyIndex = 0;

        countdown -= Time.deltaTime;

        countdown = Mathf.Clamp(countdown, 0, Mathf.Infinity);

    }

    IEnumerator SpawnMonster()
    {
        for (int i = 0; i < monsterList[enemyIndex].monsterCount; i++)
        {
            GameObject monsterObj = (GameObject)Instantiate(monsterList[enemyIndex].monsterPrefab, this.transform.position, this.transform.rotation);
            monsterObj.GetComponent<MonsterAttribute>().SetAttribute(monsterList[enemyIndex], waveCount);
            ObjectList.Add(monsterObj);
            spawnCount++;

            yield return new WaitForSeconds(0.3f);
        }

        if (waveCount % 5 == 0 && waveCount > 0)
        {
            int bossCount = Random.Range(0, bossList.Count);

            GameObject bossObj = (GameObject)Instantiate(bossList[bossCount].monsterPrefab, this.transform.position, this.transform.rotation);
            bossObj.GetComponent<MonsterAttribute>().SetAttribute(bossList[bossCount], waveCount);
            ObjectList.Add(bossObj);
            spawnCount++;

        }

        enemyIndex++;

    }

}
