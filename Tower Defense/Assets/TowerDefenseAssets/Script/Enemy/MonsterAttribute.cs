﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine;

public class MonsterAttribute : MonoBehaviour
{
    public float monsterMaxHp;
    public float monsterHP;
    public int monsterGold;
    public MonsterType type;

    public Image healthBar;
    public float hpFill;

    public bool isDead;
    public bool isSlowed;
    public bool isBurning;

    Animator anim;
    NavMeshAgent agent;
    UIManager manager;

    void Start()
    {
        manager = GameObject.FindObjectOfType<UIManager>();
        anim = this.GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();
        monsterHP = monsterMaxHp;
        isDead = false;
        isSlowed = false;
        isBurning = false;
    }

    void Update()
    {
        if (monsterHP <= 0)
        {
            this.GetComponent<Collider>().enabled = false;
            monsterHP = 0;
            isDead = true;
            anim.SetBool("isDead", true);
            agent.speed = 0;
            agent.enabled = false;
            Destroy(this.gameObject, 1.25f);
        }

        UpdateHealth();
    }

    public void SetAttribute(MonsterData mData, int waveCnt)
    {
        if (waveCnt <= 5)
        {
            monsterMaxHp = mData.monsterHp;
            monsterGold = mData.monsterGold;
            type = mData.type;
        }
        else
        {
            monsterMaxHp = mData.monsterHp * waveCnt;
            monsterGold = mData.monsterGold * waveCnt;
            type = mData.type;
        }
    }

    void UpdateHealth()
    {
        hpFill = monsterHP / monsterMaxHp;
        healthBar.fillAmount = hpFill;
    }

    public void takeDamage(float damage)
    {
        monsterHP -= damage;
    }

    void OnDestroy()
    {
        MonsterSpawner.Instance.spawnCount--;
        manager.playerGold += monsterGold;
    }
}
