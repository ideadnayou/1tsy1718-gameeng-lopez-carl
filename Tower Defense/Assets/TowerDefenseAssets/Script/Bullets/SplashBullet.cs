﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashBullet : BaseBullet
{
    public float blastRadius = 5;
    public float blastDamage;

    // Update is called once per frame
    protected override void Update()
    {
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        dir = target.position - transform.position;
        dis = speed * Time.deltaTime;

        if (dir.magnitude <= dis)
        {
            GameObject impact = Instantiate(impactEffect, transform.position, transform.rotation);
            target.GetComponent<MonsterAttribute>().takeDamage(bulletDamage);
            Destroy(impact, 1);
            Debug.Log(dis);
            Effect();
            Destroy(this.gameObject);
            
            
        }

        transform.Translate(dir.normalized * dis, Space.World);
    }

    public override void Effect()
    {
        blastDamage = bulletDamage * 0.25f;

        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, blastRadius);

        foreach (Collider item in hitColliders)
        {
            if(item.GetComponent<MonsterAttribute>())
            {
                if (item.GetComponent<MonsterAttribute>().type == MonsterType.Ground)
                {
                    item.GetComponent<MonsterAttribute>().takeDamage(blastDamage);
                }
            }
                               
        }
        
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, blastRadius);
    }

}
