﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireSplash : SplashBullet
{
    // Update is called once per frame
    protected override void Update()
    {
        base.Update();
    }

    public override void Effect()
    {
        blastDamage = bulletDamage * 0.10f;

        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, blastRadius);

        foreach (Collider item in hitColliders)
        {
            if (item.GetComponent<MonsterAttribute>())
            {
                item.GetComponent<MonsterAttribute>().takeDamage(blastDamage);

                if (item.GetComponent<Burning>())
                {
                    DestroyImmediate(item.gameObject.GetComponent<Burning>());
                    item.gameObject.AddComponent<Burning>();
                }
                else
                    item.gameObject.AddComponent<Burning>();
            }

        }
    }
}
