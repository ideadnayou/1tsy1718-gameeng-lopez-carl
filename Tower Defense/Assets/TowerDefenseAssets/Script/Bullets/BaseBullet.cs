﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseBullet : MonoBehaviour
{
    protected Transform target;

    public GameObject impactEffect;
    public float bulletDamage;
    public float speed = 70;

    protected Vector3 dir;
    protected float dis;

    protected virtual void Update()
    {
        if (target == null)
        {
            Destroy(this.gameObject);
            return;
        }
        dir = target.position - transform.position;
        dis = speed * Time.deltaTime;

        if (dir.magnitude <= dis)
        {
            GameObject impact = Instantiate(impactEffect, transform.position, transform.rotation);
            target.GetComponent<MonsterAttribute>().takeDamage(bulletDamage);
            Destroy(impact, 1);
            Destroy(this.gameObject);
        }

        transform.Translate(dir.normalized * dis, Space.World);
    }

    public virtual void Seek(Transform _target)
    {
        target = _target;
    }

    public virtual void setDamage(float damage)
    {
        bulletDamage = damage;
    }

    public virtual void Effect()
    {

    }
}
