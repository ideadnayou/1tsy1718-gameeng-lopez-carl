﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public enum TowerType
{
    Arrow,
    Cannon,
    Ice,
    Fire,
}

public enum FireRate
{
    slow = 1,
    medium = (int)2.5,
    fast = 5,
}

public enum TowerRange
{
    Short = 8,
    Medium = (int)12.5,
    Long = 15,
}

public enum TargetType
{
    Flying,
    Ground,
    All,
}

[System.Serializable]
public class TowerData
{
    public string name;
    public TowerType type;
    public FireRate rate;
    public TowerRange range;
    public TargetType targetType;
    public GameObject towerPrefab;
    public int price;
    public float buidDuration;
    public int minDamage;
    public int maxDamage;
    public float buildSpeed;
}

public class BuildingManager : MonoBehaviour
{

    public List<TowerData> towerList;
    public float buildTimer;

    Ray ray;
    RaycastHit hit;

    UIManager UIController;

    public GameObject objectTower;

    Vector3 towerPos;

    TowerCollider isPlacable;

    // Use this for initialization
    void Start()
    {
        UIController = (UIManager)GameObject.FindObjectOfType<UIManager>();
        towerPos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        SelectArea();

        if (objectTower == null) buildTimer -= Time.deltaTime;

        if (Input.GetMouseButtonDown(1) && objectTower != null)
        {
            Destroy(objectTower);
            Debug.Log("Building Canceled");
        }

        buildTimer = Mathf.Clamp(buildTimer, 0, Mathf.Infinity);
    }

    bool HasEnoughGold(TowerType type)
    {
        foreach (TowerData item in towerList)
        {
            if (item.type == type)
            {
                if (UIController.playerGold >= item.price) return true;
            }
        }

        Debug.Log("Insufficient Funds!");
        return false;

    }

    public void BuildTower(int index)
    {
        if (objectTower != null)
        {
            DestroyImmediate(objectTower);
            objectTower = null;
        }

        if (buildTimer == 0 && HasEnoughGold((TowerType)index) && objectTower == null)
        {
            GameObject tower = Instantiate(towerList[index].towerPrefab) as GameObject;
            tower.GetComponent<TowerAttribute>().SetAttribute(towerList[index]);
            isPlacable = tower.GetComponent<TowerCollider>();
            objectTower = tower;
        }
    }

    bool isOverlapping()
    {
        if (isPlacable.colliders.Count > 0)
        {
            Debug.Log("TOWER IS OVERLAPPING WITH ANOTHER OBJECT!");
            return true;
        }
        else return false;
    }

    Vector3 SnapToGrid(Vector3 towerObject)
    {
        return new Vector3(Mathf.Round(towerObject.x),
                            towerObject.y,
                            Mathf.Round(towerObject.z));

    }

    void SelectArea()
    {
        if (objectTower != null)
        {
            ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit))
            {
                towerPos = hit.point;
                objectTower.transform.position = SnapToGrid(towerPos);

                if (hit.point.y > 2.2f && !isOverlapping())
                {
                    objectTower.GetComponent<TowerScript>().Buildable();
                    if (Input.GetMouseButtonUp(0) && !EventSystem.current.IsPointerOverGameObject())
                    {
                        objectTower.GetComponent<TowerScript>().Build();
                        objectTower.transform.position = new Vector3(objectTower.transform.position.x, 0.5f, objectTower.transform.position.z);
                        buildTimer = objectTower.GetComponent<TowerAttribute>().buildDuration;
                        UIController.playerGold -= objectTower.GetComponent<TowerAttribute>().price;
                        objectTower = null;
                    }
                }
                else if (isOverlapping() || hit.point.y < 2)
                    objectTower.GetComponent<TowerScript>().NonBuildable();

                Debug.DrawLine(ray.origin, hit.point, Color.red);
            }
        }

    }
}
