﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerCollider : MonoBehaviour
{
    public List<Collider> colliders = new List<Collider>();

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Contains("Tower")) colliders.Add(col);
    }

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag.Contains("Tower")) colliders.Remove(col);
    }
}
