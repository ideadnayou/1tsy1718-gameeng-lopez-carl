﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTargeting : MonoBehaviour
{
    public Transform target;

    TowerAttribute attribute;
    TowerFiring fireManager;

    public Transform partToRotate;
    public float turnSpeed = 10;

    [SerializeField]
    List<Transform> targetList = new List<Transform>();

    // Use this for initialization
    void Start()
    {
        attribute = this.GetComponent<TowerAttribute>();
        fireManager = this.GetComponent<TowerFiring>();
        fireManager.enabled = false;
        InvokeRepeating("TargetEnemy", 0, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null)
        {
            fireManager.enabled = false;
            return;
        }
        else if (target != null) fireManager.enabled = true;

        Vector3 dir = target.position - transform.position;
        Quaternion rot = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(partToRotate.rotation, rot, Time.deltaTime * turnSpeed).eulerAngles;
        partToRotate.rotation = Quaternion.Euler(0, rotation.y, 0);
    }


    void TargetEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, (float)attribute.range);

        switch (attribute.type)
        {
            case TowerType.Cannon:
            case TowerType.Arrow:
                TargetEnemy(hitColliders);
                break;

            case TowerType.Fire:
            case TowerType.Ice:
                TargetEnemy(hitColliders, true);
                break;
            default:
                break;
        }
    }

    void TargetEnemy(Collider[] col, bool isAlwaysNearest)
    {
        AddMonsterToList(col);

        if (targetList.Count > 0)
        {
            sortList(targetList);

            target = targetList[0];

            float dis = Vector3.Distance(targetList[0].position, transform.position);

            if (dis >= (float)attribute.range)
            {
                target = null;
                targetList.Clear();
            }

            if (target != null && target.GetComponent<MonsterAttribute>().isDead)
            {
                target = null;
                targetList.Clear();
            }
        }
    }

    void TargetEnemy(Collider[] col)
    {
        AddMonsterToList(col);

        if (targetList.Count > 0)
        {
            //sort
            sortList(targetList);

            float dis = Vector3.Distance(targetList[0].position, transform.position);

            //set target
            if (dis <= (float)attribute.range && target == null)
            {
                target = targetList[0];
            }

            //target died or out of range
            if (target != null && target.GetComponent<MonsterAttribute>().isDead)
            {
                target = null;
                targetList.Clear();
            }

            if (dis >= (float)attribute.range || target == null)
            {
                target = null;
                targetList.Clear();
            }
        }
    }

    void sortList(List<Transform> items)
    {
        items.Sort(delegate (Transform t1, Transform t2)
        {
            return Vector3.Distance(t1.transform.position, transform.position).CompareTo(Vector3.Distance(t2.transform.position, transform.position));
        });
    }

    void AddMonsterToList(Collider[] col)
    {
        foreach (Collider item in col)
        {
            if (item.GetComponent<MonsterAttribute>())
            {
                switch (attribute.targetType)
                {
                    case TargetType.Flying:
                        if (item.GetComponent<MonsterAttribute>().type == MonsterType.Flying)
                        {
                            targetList.Add(item.gameObject.transform);
                        }
                        break;
                    case TargetType.Ground:
                        if (item.GetComponent<MonsterAttribute>().type == MonsterType.Ground)
                        {
                            targetList.Add(item.gameObject.transform);
                        }
                        break;
                    default:
                        targetList.Add(item.gameObject.transform);

                        break;
                }
            }
        }
    }
}
