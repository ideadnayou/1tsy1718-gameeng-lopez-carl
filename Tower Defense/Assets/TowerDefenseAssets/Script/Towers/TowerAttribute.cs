﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerAttribute : MonoBehaviour
{
    public TowerType type;
    public FireRate rate;
    public TowerRange range;
    public TargetType targetType;
    public int price;
    public float buildDuration;
    public int minDamage;
    public int maxDamage;
    public float buildSpeed;

    BuildingManager manager;
    TowerTargeting targetManager;
    TowerFiring fireManager;

    // Use this for initialization
    void Start()
    {
        manager = (BuildingManager)GameObject.FindObjectOfType<BuildingManager>();
        targetManager = this.GetComponent<TowerTargeting>();
        targetManager.enabled = false;
    }

    void Update()
    {
        if (buildDuration > 0) transform.Translate(Vector3.up * Time.deltaTime / buildSpeed);

        if (buildDuration <= 0) targetManager.enabled = true;

        if (manager.objectTower == null) buildDuration -= Time.deltaTime;

        buildDuration = Mathf.Clamp(buildDuration, 0, Mathf.Infinity);
    }

    public void SetAttribute(TowerData mData)
    {
        type = mData.type;
        rate = mData.rate;
        range = mData.range;
        targetType = mData.targetType;
        price = mData.price;
        buildDuration = mData.buidDuration;
        minDamage = mData.minDamage;
        maxDamage = mData.maxDamage;
        buildSpeed = mData.buildSpeed;
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, (float)range);
    }
}
