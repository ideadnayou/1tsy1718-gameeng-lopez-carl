﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class GameManager : MonoBehaviour {

    UIManager UIController;

    void Start()
    {
        UIController = (UIManager)GameObject.FindObjectOfType<UIManager>();
    }

    void Update()
    {
        if(UIController.isDead)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }
    
}
