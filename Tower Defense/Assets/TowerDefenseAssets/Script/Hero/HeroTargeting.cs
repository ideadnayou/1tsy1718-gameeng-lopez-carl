﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class HeroTargeting : MonoBehaviour
{
    public float range;
    public Transform target;

    List<Transform> targetList = new List<Transform>();

    NavMeshAgent agent;
    Animator anim;
    HeroMovement move;
    HeroAttribute attribute;
    HeroFiring fireManager;

    // Use this for initialization
    void Start()
    {
        anim = this.GetComponent<Animator>();
        agent = this.GetComponent<NavMeshAgent>();
        move = this.GetComponent<HeroMovement>();
        attribute = this.GetComponent<HeroAttribute>();
        fireManager = this.GetComponent<HeroFiring>();
        range = (float)attribute.range;
        fireManager.enabled = false;
        InvokeRepeating("TargetEnemy", 0, 0.5f);
    }

    // Update is called once per frame
    void Update()
    {
        if (target == null) return;

        switch (attribute.heroIndex)
        {
            case 0:
                FirstHero();
                break;

            case 1:                
            case 2:
                SecondHero();
                break;
        }

    }

    void TargetEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(this.transform.position, range);
        TargetEnemy(hitColliders);
    }

    void TargetEnemy(Collider[] col)
    {
        AddMonsterToList(col);

        if (targetList.Count > 0)
        {
            anim.SetBool("HasTarget", true);

            //sort
            sortList(targetList);

            float dis = Vector3.Distance(targetList[0].position, transform.position);

            //set target
            if (dis <= range && target == null)
            {
                target = targetList[0];
            }

            //target died or out of range
            if (target != null && target.GetComponent<MonsterAttribute>().isDead)
            {
                target = null;
                targetList.Clear();
            }

            if (dis >= range || target == null)
            {
                target = null;
                targetList.Clear();
            }
        }
        else if (targetList.Count <= 0)
        {
            anim.SetBool("HasTarget", false);
            anim.SetBool("isAttacking", false);
        }
    }

    void sortList(List<Transform> items)
    {
        items.Sort(delegate (Transform t1, Transform t2)
        {
            return Vector3.Distance(t1.transform.position, transform.position).CompareTo(Vector3.Distance(t2.transform.position, transform.position));
        });
    }

    void AddMonsterToList(Collider[] col)
    {
        foreach (Collider item in col)
        {
            if (item.GetComponent<MonsterAttribute>())
            {
                targetList.Add(item.gameObject.transform);
            }
        }
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }

    void FirstHero()
    {
        if (move.isMoving)
        {
            fireManager.enabled = false;
            anim.SetBool("isAttacking", false);
            anim.SetBool("Moving", true);
        }
        else if (!move.isMoving)
        {
            fireManager.enabled = true;
            anim.SetBool("isAttacking", true);
            anim.SetBool("Moving", false);

            Vector3 dir = target.position - transform.position;
            Quaternion rot = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, rot, Time.deltaTime * agent.angularSpeed).eulerAngles;
            this.transform.rotation = Quaternion.Euler(0, rotation.y, 0);
        }
    }

    void SecondHero()
    {
        if (move.isMoving)
        {
            fireManager.enabled = false;
        }
        else if (!move.isMoving)
        {
            fireManager.enabled = true;

            Vector3 dir = target.position - transform.position;
            Quaternion rot = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(this.transform.rotation, rot, Time.deltaTime * agent.angularSpeed).eulerAngles;
            this.transform.rotation = Quaternion.Euler(0, rotation.y, 0);
        }
    }
}
