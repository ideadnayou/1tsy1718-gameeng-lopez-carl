﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class HeroAnimation : MonoBehaviour
{
    public Animator anim;
    public HeroMovement move;

    public enum AnimationType
    {
        Idle = 0,
        Run = 1,
        Attack = 2,
        Dead = 3,

    }

    // Use this for initialization
    void Start()
    {
        anim = this.GetComponent<Animator>();
        move = this.GetComponent<HeroMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (move.isMoving) anim.SetFloat("Movement", 1);
        else if (!move.isMoving) anim.SetFloat("Movement", 0);
    }

}
