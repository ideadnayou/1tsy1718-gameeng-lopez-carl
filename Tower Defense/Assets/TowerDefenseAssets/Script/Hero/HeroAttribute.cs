﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroAttribute : MonoBehaviour
{
    public string heroName;
    public AttackRange range;
    public AttackRate rate;
    public int minDamage;
    public int maxDamage;
    public int heroIndex;

    [Header("Hero Skills")]
    public List<Ablilty> skillsList;

    public void SetAttribute(CharacterData mData)
    {
        heroName = mData.name;
        range = mData.range;
        rate = mData.rate;
        minDamage = mData.minDamage;
        maxDamage = mData.maxDamage;
        skillsList = mData.skillsList;
        heroIndex = mData.heroIndex;
    }

    public void UseSkill(int index)
    {
        skillsList[index].TriggerAbility();
    }
}