﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

public class HeroMovement : MonoBehaviour
{
    public NavMeshAgent agent;

    public GameObject pointer;
    
    private Vector3 targetPoint;
    public bool isMoving;

    GameObject cursor;

    // Use this for initialization
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        targetPoint = transform.position;
        cursor = Instantiate(pointer);
        cursor.SetActive(false);
        isMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        if (Input.GetMouseButtonDown(1) && Physics.Raycast(ray, out hit, 100.0f))
        {
            cursor.SetActive(true);
            targetPoint = hit.point;
            cursor.transform.position = targetPoint;
            isMoving = true;
        }

        agent.SetDestination(targetPoint);      

        if (transform.position == agent.destination)
        {
            cursor.SetActive(false);
            isMoving = false;
        }
    }
}
