﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum AttackRate
{
    Slow = 1,
    Medium = (int)2.5,
    Fast = 5,
}

public enum AttackRange
{
    Short = 8,
    Medium = (int)12.5,
    Long = 15,
}

[CreateAssetMenu(menuName = "Character/Hero")]
public class CharacterData : ScriptableObject
{
    public string charName;
    public GameObject heroPrefab;
    public AttackRange range;
    public AttackRate rate;
    public int minDamage;
    public int maxDamage;
    public int heroIndex;

    [Header("Hero Skills")]
    public List<Ablilty> skillsList;

}
