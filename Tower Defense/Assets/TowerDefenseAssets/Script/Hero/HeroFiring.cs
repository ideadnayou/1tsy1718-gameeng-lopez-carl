﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroFiring : MonoBehaviour
{
    HeroTargeting locate;
    HeroAttribute attribute;

    public Transform firePoint;
    public GameObject bullet;
    public float fireRate;
    public float fireCD = 0;
    public float damage;

    // Use this for initialization
    void Start()
    {
        firePoint = this.transform;
        attribute = this.GetComponent<HeroAttribute>();
        locate = this.GetComponent<HeroTargeting>();
        fireRate = (float)attribute.rate;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.enabled)
        {
            if (fireCD <= 0)
            {
                Shoot();
                fireCD = 1f / fireRate;
            }

            fireCD -= Time.deltaTime;
        }
        else if (!this.enabled)
        {
            fireCD = 0;
        }
    }

    void Shoot()
    {
        damage = Random.Range(attribute.minDamage, attribute.maxDamage);

        GameObject spwnProj = Instantiate(bullet, firePoint.position, firePoint.rotation) as GameObject;
        spwnProj.GetComponent<BaseBullet>().setDamage(damage);
        spwnProj.GetComponent<BaseBullet>().Seek(locate.target);
    }

}
