﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasRotation : MonoBehaviour {

    public Canvas enemyCanvas;

	// Use this for initialization
	void Start ()
    {
       
	}
	
	// Update is called once per frame
	void Update ()
    {
        enemyCanvas.transform.LookAt(Camera.main.transform);
	}
}
