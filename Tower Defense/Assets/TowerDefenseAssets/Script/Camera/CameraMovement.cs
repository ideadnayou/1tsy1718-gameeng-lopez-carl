﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {

    public float speed;
    public float boundary = 5;
    public float scrollSpeed;

    public Vector3 CamPos;

    // Use this for initialization
    void Start ()
    {
        CamPos = transform.position;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.mousePosition.x >= Screen.width - boundary)
        {
            CamPos.x += speed * Time.deltaTime;
        }

        if (Input.mousePosition.x <= boundary)
        {
            CamPos.x -= speed * Time.deltaTime;
        }

        if (Input.mousePosition.y >= Screen.height - boundary)
        {
            CamPos.z += speed * Time.deltaTime;
        }

        if (Input.mousePosition.y <= boundary)
        {
            CamPos.z -= speed * Time.deltaTime;
        }

        float scroll = Input.GetAxis("Mouse ScrollWheel");

        CamPos.y -= scroll * scrollSpeed * Time.deltaTime;

        CamPos.y = Mathf.Clamp(CamPos.y, 5, 20);
        CamPos.x = Mathf.Clamp(CamPos.x, 10, 55);
        CamPos.z = Mathf.Clamp(CamPos.z, -5, 45);

        transform.position = CamPos;

    }

}
