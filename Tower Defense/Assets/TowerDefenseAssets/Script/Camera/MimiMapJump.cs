﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MimiMapJump : MonoBehaviour
{

    public Camera mainCamera;
    public Camera miniMapCamera;

    public CameraMovement cam;

    public float lerpSpeed = 0.1f;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            if (miniMapCamera.pixelRect.Contains(Input.mousePosition))
            {
                Ray ray = miniMapCamera.ScreenPointToRay(Input.mousePosition);
               
                StartCoroutine(LerpToPosition(cam, new Vector3(ray.origin.x,cam.CamPos.y,ray.origin.z)));
            }
        }
    }

    IEnumerator LerpToPosition(CameraMovement target, Vector3 to)
    {
        float t = 0.0f;
        while (t < 1.0f)
        {
            t += Time.deltaTime * (Time.timeScale / lerpSpeed);
            target.CamPos = Vector3.Lerp(target.CamPos, to, t);

            yield return 0;
        }
    }
}
