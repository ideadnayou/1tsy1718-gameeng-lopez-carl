﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.AI;
using UnityEngine;

public class CharacterSelection : MonoBehaviour
{
    public List<CharacterData> heroList;

    GameObject[] characterList;

    int index;
    Vector3 rotateTo;

    void Awake()
    {
        SceneManager.LoadScene("GameData", LoadSceneMode.Additive);
    }

    void Start()
    {
        characterList = new GameObject[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
            characterList[i] = transform.GetChild(i).gameObject;

        foreach (GameObject obj in characterList)
            obj.SetActive(false);

        if (characterList[index])
            characterList[index].SetActive(true);
    }

    void Update()
    {
        this.transform.Rotate(rotateTo);
    }

    public void Toggle(bool change)
    {
        if (change)
        {
            characterList[index].SetActive(false);

            index--;

            if (index < 0)
                index = heroList.Count - 1;

            characterList[index].SetActive(true);
            rotateTo = Vector3.up;
        }
        else if (!change)
        {
            characterList[index].SetActive(false);

            index++;

            if (index == heroList.Count)
                index = 0;

            characterList[index].SetActive(true);
            rotateTo = Vector3.down;
        }
    }

    public void Confirm()
    {
        GameObject.FindObjectOfType<GameData>().mData = heroList[index];

        SceneManager.LoadSceneAsync("SampleRatio", LoadSceneMode.Additive);
        SceneManager.UnloadScene("CharacterSelect");
    }
}
