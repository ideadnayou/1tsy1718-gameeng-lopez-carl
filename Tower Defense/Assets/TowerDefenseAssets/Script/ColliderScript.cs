﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderScript : MonoBehaviour
{
    public MonsterSpawner spawnControl;
    public UIManager UIController;

    // Use this for initialization
    void Start()
    {
        spawnControl = (MonsterSpawner)GameObject.FindObjectOfType<MonsterSpawner>();
        UIController = (UIManager)GameObject.FindObjectOfType<UIManager>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag.Contains("Enemy") && this.gameObject.name.Contains("EndPosition"))
        {
            Destroy(col.gameObject);
            UIController.currentLives--;
            spawnControl.spawnCount--;
        }

        if (col.gameObject.tag.Contains("Boss") && this.gameObject.name.Contains("EndPosition"))
        {
            Destroy(col.gameObject);
            UIController.currentLives -= UIController.maxLives;
            spawnControl.spawnCount--;
        }

    }
}
