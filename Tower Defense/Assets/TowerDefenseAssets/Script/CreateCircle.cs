﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateCircle : MonoBehaviour
{
    public int segments = 100;
    public float rad;

    LineRenderer line;
    TowerAttribute attribute;

    // Use this for initialization
    void Start()
    {
        line = this.GetComponent<LineRenderer>();
        attribute = this.GetComponent<TowerAttribute>();
        rad = (float)attribute.range;

        line.SetVertexCount(segments + 1);
        line.useWorldSpace = false;
        CreatePoints();
    }

    void CreatePoints()
    {
        float x, z;
        float angle = 20;

        for (int i = 0; i < (segments + 1); i++)
        {
            x = Mathf.Sin(Mathf.Deg2Rad * angle) * rad;
            z = Mathf.Cos(Mathf.Deg2Rad * angle) * rad;

            line.SetPosition(i, new Vector3(x, 2, z));

            angle += (360 / segments);
        }
    }
}